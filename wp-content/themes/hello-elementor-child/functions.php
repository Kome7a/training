<?php 
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}

  

function hec_enqueue_scripts(){
    wp_enqueue_style("style.css", get_stylesheet_uri());
    // wp_enqueue_script("slider.js", get_stylesheet_directory_uri() . "/assets/js/slider.js");
    wp_enqueue_style("google-fonts", "https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@300;500;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300&display=swap");
}
function hec_enqueue_js(){
    wp_enqueue_script('slider.js', get_stylesheet_directory_uri() . '/assets/js/slider.js', array(), false, true);
}

add_action("wp_enqueue_scripts", "hec_enqueue_scripts");
add_action("wp_enqueue_scripts", "hec_enqueue_js");