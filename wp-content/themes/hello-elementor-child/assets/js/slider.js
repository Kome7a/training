var slideIndex = 1;

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("slidepic");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].classList.add("hidden")
    }
    slides[slideIndex - 1].classList.remove("hidden")
}

function onBurgerButtonClick() {
    var x = document.getElementById("nav-buttons");
    if (x.className === "home-buttons") {
        x.className += " responsive";
    } else {
        x.className = "home-buttons";
    }
}


// Next/previous controls
function plusSlides(n) {
    showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
    showSlides(slideIndex = n);
}

function onContentLoaded() {
    showSlides(slideIndex);
}

document.addEventListener("DOMContentLoaded", onContentLoaded);