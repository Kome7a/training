<!-- Navbar -->

<div class="navbar">
    <img src="<?php bloginfo('template_url')?>/assets/images/LOGOTYPE.png" class="icon" id="header-icon"> 
    <div class="home-buttons" id="nav-buttons">
       <ul>
       <div class="collapse" onclick="onBurgerButtonClick()">&equiv;</div>
        <li class="btn"><a href="#">Home</a></li>
        <li class="btn"><a href="#">Work</a></li>
        <li class="btn"><a href="#">About</a></li>
        <li class="btn"><a href="#">Blog</a></li>
        <li class="btn"><a href="#">Contact</a></li>
       </ul>
    </div>
</div>

<!-- Slider section -->
<div class="slider">
    <div class="picnav previous">
        <a class="prev" onclick="plusSlides(1)">
            <span class="left"></span>
        </a>
    </div>
    <ul class="slide-show">
        <li class="slidepic"><img id="pic1" src="<?php bloginfo('template_url') ?>/assets/images/Main.png"></li>
        <li class="slidepic"><img id="pic2" src="<?php bloginfo('template_url') ?>/assets/images/mountain.jpg"></li>
        <li class="slidepic"><img id="pic3" src="<?php bloginfo('template_url') ?>/assets/images/view.jpg"></li>
    </ul>
    <div class="picnav next">
        <a class="next" onclick="plusSlides(-1)">
        <span class="right"></span>
        </a>
    </div>
    <div class="centered">
        <h1>Aero</h1>
    </div>
    <h3 class="centered">The one and only Aerial Photography Studio</h3>
</div>

<!-- 'We are aero' section -->

<div class="div-center white">
    <h2 class="hire-heading">we are aero</h2>
    <p class="description">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut libero ligula, posuere ut molestie eu, euismod in leo. Pellentesque varius vel dolor eu suscipit. Maecenas condimentum ante at elit posuere, a pharetra lacus pharetra. Suspendisse interdum tristique neque, ut maximus risus tempor nec. Morbi eu placerat erat. Suspendisse potenti. Morbi rhoncus nibh sit amet maximus tempus. Duis mattis dui urna, eget pulvinar elit lobortis vitae.</p>
</div>


<!-- LATEST POSTS section -->

<div class="latest-posts div-center">
    <h2 class="latest-posts-heading">Latest Posts</h2>
    <div class="flex-container">
        <div class="img-container">
            <img src="<?php bloginfo('template_url') ?>/assets/images/9.JPG">
            <h2 class="title post-title flex">California<span class="read-all-small"><a href="#" class="read-all">Read</a></span></h2>
            <div class="img-description hidden">
                <h2 class="post-title">California</h2>
                <p class="date">05 Sep 2016</p>
                <p class="img-text">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut libero ligula, posuere ut molestie eu, euismod in leo. Pellentesque varius vel dolor eu suscipit.</p>
                <a href="#" class="read-all">Read all</a>
            </div>
        </div>
        <div class="img-container">
            <img src="<?php bloginfo('template_url') ?>/assets/images/5.jpg">
            <h2 class="title post-title flex">Oregon Storm<span class="read-all-small"><a href="#" class="read-all">Read</a></span></h2>
            <div class="img-description hidden">
                <h2 class="post-title">Oregon Storm</h2>
                <p class="date">10 Sep 2016</p>
                <p class="img-text">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut libero ligula, posuere ut molestie eu, euismod in leo. Pellentesque varius vel dolor eu suscipit.</p>
                <a href="#" class="read-all">Read all</a>
            </div>
        </div>
        <div class="img-container">
            <img src="<?php bloginfo('template_url') ?>/assets/images/8.jpg">
            <h2 class="post-title title flex">Japan<span class="read-all-small"><a href="#" class="read-all">Read</a></span></h2>
            <div class="img-description hidden">
                <h2 class="post-title">Japan</h2>
                <p class="date">05 Oct 2016</p>
                <p class="img-text">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut libero ligula, posuere ut molestie eu, euismod in leo. Pellentesque varius vel dolor eu suscipit.</p>
                <a href="#" class="read-all">Read all</a>
            </div>
        </div>
    </div>
</div>


<!-- Why hire us section -->

<div class="div-center white">
    <h2>Why hire us</h2>
    <div class="row">
        <div class="column">
            <img id="contractor" src="<?php bloginfo('template_url') ?>/assets/images/contractor_icn@2x.png">
            <p id="contractor">Over 200 contractors in over <br> 30 states and 20 countries</p>
        </div>
        <div class="column">
            <img src="<?php bloginfo('template_url') ?>/assets/images/fleet_icn@2x.png">
            <p class="text-align">Fleet of 5 helicopters <br> and 2 ultralight planes</p>
        </div>
    </div>
    <div class="row">
         <div class="column">
             <img class="f-left" src="<?php bloginfo('template_url') ?>/assets/images/camera_icn@2x.png">
             <p class="text-align">We use the best gear <br>
               available in the world</p>
         </div>
         <div class="column"> 
             <img class="f-left" src="<?php bloginfo('template_url') ?>/assets/images/helmet_icn@2x.png">
             <p>No accidents since we <br>started the service in 1986</p>
        </div>
        </div>
    </div>
</div>



<!-- Our clients section -->

<div class="div-center">
    <h2 class="hire-heading latest-posts-heading">Our clients</h2>
    <div class="our-clients">
        <div class="row">
            <span><img class="logo" src="<?php bloginfo('template_url') ?>/assets/images/fox_logo@2x.png"></span>
            <span><img class="logo" src="<?php bloginfo('template_url') ?>/assets/images/hbo_logo@2x.png"></span>
            <span><img class="logo" src="<?php bloginfo('template_url') ?>/assets/images/rapha_logo@2x.png"></span>
            <span><img class="logo" src="<?php bloginfo('template_url') ?>/assets/images/apple_logo@2x.png"></span>
            </div>
        </div>   
    </div>
    
</div>



<!-- FROM OUR CLIENTS -->

<div class="div-center white">
    <h2 class="hire-heading">From our clients</h2>

    <div class="row">
        <div class="column" id="wrap">
        <img src="<?php bloginfo('template_url') ?>/assets/images/profilepic@2x.png">
        <p id="comment">" Ut sit amet tortor ut dui tempus pulvinar. Vivamus dapibus massa luctus tellus aliquam egestas. 
            <br> In arcu quam, efficitur id est ut, dignissim mattis mauris. Aenean eu tellus eu nisl sagittis mattis vel vel nisi.
            <br> Mauris mattis fringilla faucibus. Aenean ac neque scelerisque, dignissim nulla eu. "</p>
        </div>
    </div>
    <p id="info"><span id="name">Joel Holmes</span> Director of Photography / HBO</p>
</div>
    <div class="nav-dots">
        <a class="dot active" href=""></a>
        <a class="dot" href=""></a>
        <a class="dot" href=""></a>
    </div>



<!-- HOW WAS THAT section -->

<div class="f-div-center latest-posts-heading">
    <h2 class="hire-heading">How was that</h2>
    <p class="description">Morbi bibendum viverra auctor. Phasellus posuere placerat leo eget condimentum. Sed posuere congue nisi.
        <br>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla nec pulvinar nibh. 
        <br>In eu lacus ac est mattis faucibus nec eu augue. Proin vestibulum sit amet felis eget accumsan. Nunc vitae risus euismod, 
        <br>dignissim lorem at, placerat nunc. Pellentesque consectetur nulla nec ante commodo tristique. 
        <br>Suspendisse lacus lacus, euismod ac risus ac, suspendisse viverra tortor vel convallis pulvinar.</p>
    <div class="row-btns">
        <a href="" class="btn">About</a>
        <a href="" class="btn">Contact</a>
    </div>
</div>

<!-- FOOTER -->

<div class="div-center white">
    <img src="<?php bloginfo('template_url') ?>/assets/images/AEPO Logo - Footer@2x.png">
    <p class="all-rights">All rights reserved, &#169;2016 AEPO inc</p>
    <ul class="flex">
        <li class="f-btn"><a href="#">Home</a></li>
        <li class="f-btn"><a href="#">Work</a></li>
        <li class="f-btn"><a href="#">About</a></li>
        <li class="f-btn"><a href="#">Blog</a></li>
        <li class="f-btn"><a href="#">Contact</a></li>  
        <li class="f-btn"><a href="#">FAQ</a></li>  
        <li class="f-btn"><a href="#">Legal</a></li>  
    </ul>
</div>